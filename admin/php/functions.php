<?php 
date_default_timezone_set('Asia/Karachi');
$con = mysql_connect('localhost', 'root', '') or die(mysql_error());
$db = mysql_select_db('fes', $con) or die(mysql_error());
include ('../access.php');
//CHECK LOGIN
if($page!='index')
{
	if($_SESSION['faculty id'] == NULL && $_SESSION['faculty login'] != 1)
	{
		header('Location: index.php');
	}
}
if($section=='course')
{
	//GET CURRENT COURSE CODE
	if(isset($_GET['course']))
	{
		$_GET['course']=strtoupper ($_GET['course']);
		if($_GET['course']==$_SESSION['faculty course1'] || $_GET['course']==$_SESSION['faculty course2'] || $_GET['course']==$_SESSION['faculty course3'] || $_GET['course']==$_SESSION['faculty course4'])
		{
			$_SESSION['current course code']=$_GET['course'];
		}
		else
		{
			$_SESSION['current course code']=$_SESSION['faculty course1'];
			echo'You do not have access to '.$_GET['course'].'. You can only access the courses which you are teaching. In case of any problem please contact at u2010252@giki.edu.pk. <a href="javascript:history.back()">Click Here</a> to go back.';
			die();
		}
	}

	else
	{	
		header('Location:?course='.$_SESSION['faculty course1']);
	}

	//GET CURRENT COURSE TITLE
	$course_select_query = mysql_query("SELECT * FROM `fes`.`coursesoffered` WHERE `course code` = '".$_SESSION['current course code']."'");
	$course=mysql_fetch_array($course_select_query);
	$_SESSION['current course title']=$course['course title'];

	//CONNECT TO CURRENT COURSE
	$db_course = mysql_select_db($_SESSION['current course code'], $con);
}

//FINDING FACULTY TYPE FOR PREVIEW PROFILE LINK
$profile_select_query = mysql_query("SELECT * FROM `fes`.`profiles` WHERE `username` = '".$_SESSION['faculty username']."'");
$profile=mysql_fetch_array($profile_select_query);
$_SESSION['faculty type']=$profile['type'];
//FILE HANDLING

//RELATIVE PATH 
$admin_path_to_files="../CourseWare/files/".$_SESSION['current course code']."/";

function check_exist_upload_file($filename,$admin_path_to_files)
{	
	$admin_url=$admin_path_to_files.$filename; 

	if ($_FILES["file"]["error"] > 0)
	{
		$return_array['error']="File Upload Error: ".$_FILES["file"]["error"];
		return $return_array;
	}
	else
	{
		if (file_exists($admin_url))
		{
			$i=0;
			do
			{
				$i++;
				$admin_url=$admin_path_to_files.'('.$i.')'.$filename; 
			}
			while(file_exists($admin_url));
			$return_array['warning']=$filename.' already existed, so it was automatically renamed to <b>('.$i.')'.$filename.'</b>';
			$filename='('.$i.')'.$filename;
			$admin_url=$admin_path_to_files.$filename; 
			
		}
		if(!move_uploaded_file($_FILES["file"]["tmp_name"], $admin_url))
		{
			$return_array['error']="File Upload Error: Unable to move file.";
		}
		$return_array['file']=$filename;
		return $return_array;
	}
}

function delete_file($filename,$admin_path_to_files)
{
	$admin_url=$admin_path_to_files.$filename;
	
	if($filename)
	{
		if (file_exists($admin_url))
		{ 
			$return_array['error']=NULL;
			if(!unlink($admin_url))
			{
				$return_array['error']="File Delete Error: Unable to delete file.";
			}		
			return $return_array;
		}
		else
		{	
			//if file is already deleted
			$return_array['error']=NULL;
			
		}
	}
	return $return_array; 
}
function uploadphoto($filename,$facultyname)
{	
	if ($_FILES["file"]["error"] > 0)
	{
		$return_array['error']="Photo Upload Error: ".$_FILES["file"]["error"];
		return $return_array;
	}
	else
	{		
		$extension = end(explode(".", $filename));
		
		if($extension=='jpg' || $extension=='jpeg' || $extension=='JPG' || $extension=='JPEG')
		{
			$url='../faculty/photos/'.$facultyname.'.'.$extension;
			if (file_exists($url))
			{
				if(!unlink($url))
				{
					$return_array['error']="Can not upload photo: 1";
					return $return_array; 
				}
			}
			if(!move_uploaded_file($_FILES["file"]["tmp_name"], $url))
			{		
				$return_array['error']="Can not upload photo: 2";
			}
		}
		else
		{
			$return_array['error']="Photo must be in .jpeg format.";
		}			
		return $return_array;			
	}
}

function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
	//check ip from share internet
	{
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	//to check ip is pass from proxy
	{
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}
?>
