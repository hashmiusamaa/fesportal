<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='course';
$page='Overview';
$tablename='overview';

require('php/functions.php');
$pagetitle=$_SESSION['current course code'].' - '.$_SESSION['current course title'];

//actions
if($_POST['save'])
{ 
	
	$save['course code']=$_SESSION['current course code'];
	$save['course title']=$_SESSION['current course title'];
	
	$save['instructor']=mysql_real_escape_string($_POST['instructor']);
	$save['credit hours']=mysql_real_escape_string($_POST['credit_hours']);
	$save['course description']=mysql_real_escape_string($_POST['course_description']);
	$save['pre requisite']=mysql_real_escape_string($_POST['pre_requisite']);
	$save['class schedule']=mysql_real_escape_string($_POST['class_schedule']);
	$save['teaching assistant']=mysql_real_escape_string($_POST['teaching_assistant']);
	$save['grading policy']=mysql_real_escape_string($_POST['grading_policy']);	
		
	$save_query=mysql_query("UPDATE `".$_SESSION['current course code']."`.`".$tablename."` SET `course code` = '".$save['course code']."', `instructor` = '".$save['instructor']."', `credit hours` = '".$save['credit hours']."', `course description` = '".$save['course description']."', `pre requisite` = '".$save['pre requisite']."', `class schedule` = '".$save['class schedule']."',`teaching assistant` = '".$save['teaching assistant']."', `grading policy` = '".$save['grading policy']."' LIMIT 1;");

	$_SESSION['success']='Changes have been saved.';
	$_POST=NULL;
	header('refresh:0'); die();
	
}

//extract entries from database
$entries=mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename."`");
$entry=mysql_fetch_array($entries);

//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Overview</h1>
				<div id="form-wrapper">
				<?php 
				if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
				if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
				if($success){ 
					echo'<span class="message">'.$success.'</span>';}
					
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">';
					if($_POST['edit'] || $_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Course Code:</span></td><td><span class="label">'.$_SESSION['current course code'].'</span></td></tr>
							<tr><td><span class="label">Course Title:</span></td><td><span class="label">'.$_SESSION['current course title'].'</span></td></tr>
							<tr><td><span class="label">Instructor:</span></td><td><input type="text" name="instructor" value="'.$entry['instructor'].'"></td></tr>
							<tr><td><span class="label">Credit Hours:</span></td><td><textarea name="credit_hours">'.$entry['credit hours'].'</textarea></td></tr>
							<tr><td><span class="label">Course Description:</span></td><td><input type="text" name="course_description" value="'.$entry['course description'].'"></td></tr>
							<tr><td><span class="label">Pre-requisite:</span></td><td><input type="text" name="pre_requisite" value="'.$entry['pre requisite'].'"></td></tr>
							<tr><td><span class="label">Class Schedule:</span></td><td><input type="text" name="class_schedule" value="'.$entry['class schedule'].'"></td></tr>
							<tr><td><span class="label">Teaching Assistant:</span></td><td><input type="text" name="teaching_assistant" value="'.$entry['teaching assistant'].'"></td></tr>
							<tr><td><span class="label">Grading Policy:</span></td><td><input type="text" name="grading_policy" value="'.$entry['grading policy'].'"></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="save" value="Save" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>';
					}
					else if(!$_POST['edit'] && !$_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Course Code:</span></td><td><span class="label">'.$_SESSION['current course code'].'</span></td></tr>
							<tr><td><span class="label">Course Title:</span></td><td><span class="label">'.$_SESSION['current course title'].'</span></td></tr>
							<tr><td><span class="label">Instructor:</span></td><td><span class="label">'.$entry['instructor'].'</span></td></tr>
							<tr><td><span class="label">Credit Hours:</span></td><td><span class="label">'.$entry['credit hours'].'</span></td></tr>
							<tr><td><span class="label">Course Description:</span></td><td><span class="label">'.$entry['course description'].'</span></td></tr>
							<tr><td><span class="label">Pre-requisite:</span></td><td><span class="label">'.$entry['pre requisite'].'</span></td></tr>
							<tr><td><span class="label">Class Schedule:</span></td><td><span class="label">'.$entry['class schedule'].'</span></td></tr>
							<tr><td><span class="label">Teaching Assistant:</span></td><td><span class="label">'.$entry['teaching assistant'].'</span></td></tr>
							<tr><td><span class="label">Grading Policy:</span></td><td><span class="label">'.$entry['grading policy'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="edit" value="Edit" /></td></tr>';
					}
				?>

						</form>
					</table>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$_SESSION['current course code']=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
//destroy session current course
include('php/foot.php'); ?>
