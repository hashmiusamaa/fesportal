<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='account';
$page='Change Password';

require('php/functions.php');
$pagetitle='Account Control';

//actions
if($_POST['change'])
{ 
	$change['current password']=mysql_real_escape_string($_POST['current_password']);
	$change['new password']=mysql_real_escape_string($_POST['new_password']);
	$change['confirm new password']=mysql_real_escape_string($_POST['confirm_new_password']);
	
	if(!$change['current password'])
	{
		$_SESSION['error']='Please enter your current password.';
	}
	else if(!$change['new password'])
	{
		$_SESSION['error']='Please enter new password.';
	}
	else if($change['new password']!=$change['confirm new password'])
	{
		$_SESSION['error']='New passwords do not match. Please enter again.';
	}
	else
	{
		$faculty_select_query=mysql_query("SELECT * FROM `fes`.`faculty` WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;");
		$faculty=mysql_fetch_array($faculty_select_query);
		if($faculty['password']!=$change['current password'])
		{
			$_SESSION['error']='Current password does not match. Please try again.';			
		}
		else
		{
			$change_query= mysql_query("UPDATE `fes`.`faculty` SET `password` = '".$change['new password']."' WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;");	
			$_SESSION['success']='You password has been changed.';
			$_POST=NULL;
			header('refresh:0'); die();			
		}
	}
}

//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Change Password</h1>
				<div id="form-wrapper">
				<?php 
				if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
				if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
				if($success){ 
					echo'<span class="message">'.$success.'</span>';}
				?>	
					<table>
						<form action="" method="post" enctype="multipart/form-data">
							<tr><td width="135px"><span class="label">Current Password:</span></td><td><input type="password" name="current_password"></td></tr>
							<tr><td><span class="label">New Password:</span></td><td><input type="password" name="new_password"></td></tr>
							<tr><td><span class="label">Confirm New Password:</span></td><td><input type="password" name="confirm_new_password"></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="change" value="Change" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>
						</form>
					</table>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
include('php/foot.php'); ?>
