<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='edit profile';
$page='Research Areas';
$tablename='profiles';

require('php/functions.php');
$pagetitle='Edit Profile';

//actions
if($_POST['save'])
{ 
	$save['research areas']=mysql_real_escape_string($_POST['research_areas']);
	
	$save_query_profiles=mysql_query("UPDATE `fes`.`".$tablename."` SET `research` = '".$save['research areas']."' WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;"); 
	$_SESSION['success']='Changes have been saved.';
	$_POST=NULL;
	header('refresh:0'); die();
	
}

//extract entries from database
$faculty_select_query=mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;");
$faculty=mysql_fetch_array($faculty_select_query);

//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Research Areas</h1>
				<div id="form-wrapper">
				<?php 
				if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
				if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
				if($success){ 
					echo'<span class="message">'.$success.'</span>';}
					
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">';
					if($_POST['edit'] || $_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Research Areas:</span></td><td><textarea class="ckeditor" name="research_areas">'.$faculty['research'].'</textarea></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="save" value="Save" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>';
					}
					else if(!$_POST['edit'] && !$_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Research Areas:</span></td><td><span class="label">'.$faculty['research'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="edit" value="Edit" /></td></tr>';
					}
				?>

						</form>
					</table>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
include('php/foot.php'); ?>
