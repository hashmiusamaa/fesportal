<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='course';
$page='Projects';
$tablename='projects';

require('php/functions.php');
$pagetitle=$_SESSION['current course code'].' - '.$_SESSION['current course title'];

//actions
if($_POST['add'])
{ 

	$add['title']=mysql_real_escape_string($_POST['title']);
	$add['deadline']=mysql_real_escape_string($_POST['deadline']);
	$add['task']=mysql_real_escape_string($_POST['task']);
	$add['details']=mysql_real_escape_string($_POST['details']);
	$add['file']=$_FILES["file"]["name"];

	if(!$add['title'])
	{
		$_SESSION['error']='Please enter a title to add an entry.';
	}
	else if(!$add['details'] && !$add['task'])
	{
		$_SESSION['error']='Please enter details or task to add an entry.';
	}
	else
	{	
		if($add['file'])
		{
			$function_return=check_exist_upload_file($add['file'],$admin_path_to_files);
			
			$_SESSION['warning']=$function_return['warning'];
			$_SESSION['error']=$function_return['error'];
			
			$add['file']=$function_return['file'];
			$add['url']=mysql_real_escape_string(rawurlencode($add['file']));
			$add['file']=mysql_real_escape_string($add['file']);
		}
		if(!$_SESSION['error'])
		{
			$add_query=mysql_query("INSERT INTO `".$_SESSION['current course code']."`.`".$tablename."` (`title`, `deadline`, `task`, `details`, `file`, `url`, `posted`) VALUES('".$add['title']."','".$add['deadline']."','".$add['task']."','".$add['details']."','".$add['file']."','".$add['url']."','".date("d/m/y : H:i:s", time())."')");
			$_SESSION['success']='Entry has been added.';
			$_POST=NULL;
			header('refresh:0'); die();
		}
	}
}

if($_POST['delete'])
{ 
	$delete['row id']=$_POST['id'];

	$delete_select_query = mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename."` WHERE `row id` = '".$delete['row id']."' LIMIT 1;");
	$delete = mysql_fetch_array($delete_select_query);
	if($delete['file'])
	{
		$function_return=delete_file($delete['file'],$admin_path_to_files);
		$_SESSION['error']=$function_return['error'];
	}
	if(!$_SESSION['error'])
	{
		$delete_query=mysql_query("DELETE FROM `".$_SESSION['current course code']."`.`".$tablename."` WHERE `row id` = '".$delete['row id']."' LIMIT 1;");
		$_SESSION['success']='Entry has been deleted.';
		$_POST=NULL;
		header('refresh:0'); die();
	}
}

if($_POST['edit'])
{ 
	$edit['row id']=$_POST['id'];

	$edit_select_query = mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename."` WHERE `row id` = '".$edit['row id']."' LIMIT 1;");
	$edit = mysql_fetch_array($edit_select_query);

	$_POST['title']=$edit['title'];
	$_POST['deadline']=$edit['deadline'];
	$_POST['task']=$edit['task'];
	$_POST['details']=$edit['details'];
	$_SESSION['row id']=$edit['row id'];
	$_SESSION['file']=$edit['file'];
	
}
if($_POST['save'])
{ 
	$save['title']=mysql_real_escape_string($_POST['title']);
	$save['deadline']=mysql_real_escape_string($_POST['deadline']);
	$save['task']=mysql_real_escape_string($_POST['task']);
	$save['details']=mysql_real_escape_string($_POST['details']);
	$save['file']=$_FILES["file"]["name"];
	$save['old file']=$_SESSION['file'];
	$save['row id']=$_SESSION['row id'];
	
	if(!$save['title'])
	{
		$_SESSION['error']='Please enter a title to save an entry.';
	}
	else if(!$save['details'] && !$save['task'])
	{
		$_SESSION['error']='Please enter details or task to save an entry.';
	}
	else
	{	
		
		if($save['file'])
		{
			$function_return=delete_file($save['old file'],$admin_path_to_files);
			$_SESSION['error']=$function_return['error'];
		
			if(!$_SESSION['error'])
			{
				$function_return=check_exist_upload_file($save['file'],$admin_path_to_files);
				$_SESSION['warning']=$function_return['warning'];
				$_SESSION['error']=$function_return['error'];
				
				$save['file']=$function_return['file'];
				$save['url']=mysql_real_escape_string(rawurlencode($save['file']));
				$save['file']=mysql_real_escape_string($save['file']);
				
				if(!$_SESSION['error'])
				{
					$save_query=mysql_query("UPDATE `".$_SESSION['current course code']."`.`".$tablename."` SET `title` = '".$save['title']."', `deadline` = '".$save['deadline']."', `task` = '".$save['task']."',`details` = '".$save['details']."', `file` = '".$save['file']."', `url` = '".$save['url']."',`posted` = '".date("d/m/y : H:i:s", time())."' WHERE `row id` = '".$save['row id']."' LIMIT 1;");
				}
			}
		}
		else
		{			
			if(!$_SESSION['error'])
			{
				$save_query=mysql_query("UPDATE `".$_SESSION['current course code']."`.`".$tablename."` SET `title` = '".$save['title']."', `deadline` = '".$save['deadline']."', `task` = '".$save['task']."', `details` = '".$save['details']."', `posted` = '".date("d/m/y : H:i:s", time())."' WHERE `row id` = '".$save['row id']."' LIMIT 1;");

			}
		}
		$_SESSION['success']='Entry has been edited.';
		$_POST=NULL;
		$_SESSION['old file']=NULL;
		$_SESSION['row id']=NULL;
		header('refresh:0'); die();
	}
}

//extract entries from database
$entries=mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename."` ORDER BY `row id` DESC");
$entry=mysql_fetch_array($entries);

//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Projects</h1>
				
				<div id="form-wrapper"> 
				<h2>Add New Project:</h2>
					<?php if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
					if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
					if($success){ 
					echo'<span class="message">'.$success.'</span>';}?>

					<table>
						<form action="" method="post" enctype="multipart/form-data">
						<tr><td width="135"><span class="label">Title:</span></td><td><input type="text" name="title" value="<?php echo $_POST['title'];?>"></td><td width="135"><span class="label">Deadline:</span></td><td><input type="text" name="deadline" value="<?php echo $_POST['deadline'];?>"></td></tr>
						<tr><td><span class="label">Task:</span></td><td><textarea name="task"><?php echo $_POST['task'];?></textarea></td>
						<td><span class="label">Details:</span></td><td><textarea name="details"><?php echo $_POST['details'];?></textarea></td></tr>
						<tr><td><span class="label">Attachement:</span></td><td><input class="file" type="file" name="file" id="file" /><br /><span class="label">Max File Size: 150MB</span></td></tr>
						<?php 
						if($_POST['edit'] || $_POST['save'])
						{
						echo'<tr><td></td><td><input class="button" type="submit" name="save" value="Save" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>';
						}
						else
						{
						echo'<tr><td></td><td><input class="button" type="submit" name="add" value="Add" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>';
						}
						?>
						</form>
					</table>
				</div><!--form-wrapper-->
				
				<?php 
				if(!$_POST['edit'] && !$_POST['save'])
				{
				echo'<div id="sliding-list">';
				do
				{
					if(!$entry)
					{
					echo '
					<h2>No project added.</h2>'; 
					} 
					else 
					{
					echo'
					<li class="closeitem expandable">'.$entry['title'].'</li>
					<ul class="listcontent">
						<li><h2>Title:</h2><p>'.$entry['title'].'</p></li>';
						if($entry['deadline'])
						{
						echo'<li><h2>Deadline:</h2><p>'.$entry['deadline'].'</p></li>';
						}
						if($entry['task'])
						{
						echo'<li><h2>Task:</h2><p>'.$entry['task'].'</p></li>';
						}
						if($entry['details'])
						{
						echo'<li><h2>Details:</h2><p>'.$entry['details'].'</p></li>';
						}
						if($entry['file'])
						{
						echo'
						<li><h2>Attachment:</h2><p><a href="../CourseWare/files/'.$_SESSION['current course code'].'/'.$entry['url'].'">'.$entry['file'].'</a></p></li>';
						}
						echo'<li><span class="posted">Posted on '.$entry['posted'].'</span></li>
						<form action="" method="post">
							<input type="hidden" name="id" value="'.$entry['row id'].'" />
							<input class="button" type="submit" name="edit" value="Edit" />
							<input class="button" type="submit" name="delete" value="Delete" />
						</form>	
					</ul>';				
					}
				}
				while ($entry = mysql_fetch_array($entries));
								
				echo'
				</div><!--sliding-list-->';
				}?>
			
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$_SESSION['current course code']=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
//destroy session current course
include('php/foot.php'); ?>
