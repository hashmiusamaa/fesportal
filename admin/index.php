<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='home';
$page='index';

require('php/functions.php');
$pagetitle='Faculty Panel';

//actions
if($_POST['login']){
	
	$login['username']=mysql_real_escape_string($_POST['username']);
	$login['password']=mysql_real_escape_string($_POST['password']);

	if(!$login['username'])
	{
		$error='Please enter Username';
    }
	else if(!$login['password'])
	{
		$error='Please enter Password';
    }
	else
	{
		$user_select_query = mysql_query("SELECT * FROM `fes`.`faculty` WHERE `username` = '".$login['username']."'");
		$user_found = mysql_num_rows($user_select_query);
		if($user_found == 0)
		{
			$error='The Username is invalid. Please enter the correct Username.';
		}
		else
		{
			$user=mysql_fetch_array($user_select_query);
			if($login['password']!=$user['password'])
			{
				$error='The Password is invalid. Please enter the correct Password.';
			}
			else
			{
				$_SESSION['faculty id'] = $user['id'];
				$_SESSION['faculty name'] = $user['name'];
				$_SESSION['faculty username'] = $user['username'];
				
				$_SESSION['faculty course1'] = $user['course1'];
				$_SESSION['faculty course2'] = $user['course2'];
				$_SESSION['faculty course3'] = $user['course3'];
				$_SESSION['faculty course4'] = $user['course4'];
				
				$_SESSION['faculty login']=1;
				
				$login['lastonline'] = date("d/m/y : H:i:s", time());
				$last_online_update_query=mysql_query("UPDATE `fes`.`faculty` SET `last online` = '".$login['lastonline']."' WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;");

				header('Location: index.php');
			}
		}
	}
}
?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper"><?php
				if($_SESSION['faculty login']==1)
				{
echo'
					<h1>Welcome '.$_SESSION['faculty name'].',</h1>
					<div id="text-wrapper" >
						<p>This Faculty Panel provides an online system for the instructors of Faculty of Engineering Sciences, that makes creation, management, and use of course materials and communication among students and instructors more effective and efficient. It also enable the instructors to keep their profile up to date.</p>
						<p>This panel will allow you to edit your profile and upload photo, both of them will be displayed on the faculty website. In addition to this you will be also able to upload course material, announce quizzes, post details about assignments and projects, and update attendance record for students. This all material related to course will be available to students at Student CourseWare and they can access it easily. The courses that you teach are listed on left of this page.</p> 
						<p>If you find any bug/error in this panel system please inform us by going to the <a href="ReportBug.php">Report a Bug</a> page. It will help us in improving this system.</p>					
					</div><!--text-wrapper-->';
				}
				else
				{
				echo '
				<h1>Login Here</h1>
				<div id="login-wrapper"> 
					<h2>Please enter your Username and Password</h2>
					<span class="message">'.$error.'</span>
					<table>
						<form action="" method="post" enctype="multipart/form-data">
						<tr><td><span class="label">Username:</span></td><td><input type="text" name="username" value="'.$_POST['username'].'"></td></tr>
						<tr><td><span class="label">Password:</span></td><td><input type="password" name="password"></td></tr>
						<tr><td></td><td><input class="button" type="submit" name="login" value="Login" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>
						</form>
					</table>
				</div><!--login-wrapper-->
				';
				}
				?>
			
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
include('php/foot.php'); ?>
