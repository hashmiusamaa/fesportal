<?php 
date_default_timezone_set('Asia/Karachi');
$con = mysql_connect('localhost', 'root', '') or die(mysql_error());
$db = mysql_select_db('fes', $con) or die(mysql_error());
include ('../access.php');
//CHECK LOGIN	
if($page!='index')
{
	if($page!='Forgot Your Password' && $page!='Reset Your Password' && $page!='Create an Account' && $_SESSION['user id'] == NULL && $_SESSION['user login'] != 1)
	{
		header('Location: index.php');
	}
	if(($page=='Forgot Your Password' || $page=='Reset Your Password' || $page=='Create an Account') && $_SESSION['user id'] != NULL && $_SESSION['user login'] == 1)
	{
		header('Location: index.php');
	}
}
if($section=='course')
{
	//GET CURRENT COURSE CODE
	if(isset($_GET['course']))
	{
		$_GET['course']=strtoupper ($_GET['course']);
		$find_course = mysql_query("SELECT * FROM `fes`.`coursesenrolled` WHERE `id` = '".$_SESSION['user id']."' AND `username` = '".$_SESSION['user username']."' AND `course code` = '".$_GET['course']."'"); 
		$num = mysql_num_rows($find_course);
		if($num >= 1)
		{
			$_SESSION['current course code']=$_GET['course'];
		}
		else
		{
			$_SESSION['course error']=' The course code <b>'.$_GET['course'].'</b> is invalid or you might not have not joined the course. Please go to <a href="index.php">CourseWare Lobby</a> to join the course.';
		}
	}

	else
	{	
		header('Location:index.php');
	}

	if($_SESSION['current course code'])
	{
		//GET CURRENT COURSE TITLE
		$course_select_query = mysql_query("SELECT * FROM `fes`.`coursesoffered` WHERE `course code` = '".$_SESSION['current course code']."'");
		$course=mysql_fetch_array($course_select_query);
		$_SESSION['current course title']=$course['course title'];

		//CONNECT TO CURRENT COURSE
		$db_course = mysql_select_db($_SESSION['current course code'], $con);
	}
}

//FILE HANDLING

//RELATIVE PATH 
$admin_path_to_files="../courseware/".$_SESSION['current course code']."/files/";
function uploadphoto($filename,$oldphotoname,$newphotoname)
{	
	if ($_FILES["file"]["error"] > 0)
	{
		$return_array['error']="Photo Upload Error: ".$_FILES["file"]["error"];
		return $return_array;
	}
	else
	{		
		$extension = end(explode(".", $filename));
		
		if($extension=='jpg' || $extension=='jpeg')
		{
			$oldurl='photos/'.$oldphotoname.'.'.$extension;
			$newurl='photos/'.$newphotoname.'.'.$extension;
			if ($oldphotoname!='default')
			{
				if (file_exists($oldurl))
				{
					if(!unlink($oldurl))
					{
						$return_array['error']="Can not upload photo: 1";
						return $return_array; 
					}
				}
			}
			if(!move_uploaded_file($_FILES["file"]["tmp_name"], $newurl))
			{		
				$return_array['error']="Can not upload photo: 2";
			}
			
		}
		else
		{
			$return_array['error']="Photo must be in .jpeg format.";
		}			
		return $return_array;			
	}
}

function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
	//check ip from share internet
	{
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	//to check ip is pass from proxy
	{
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}
?>
