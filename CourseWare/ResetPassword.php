<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='lobby';
$page='Reset Your Password';
$tablename='users';
$tablename1='forgotrequest';

require('php/functions.php');
$pagetitle='Student CourseWare';

//$faculties = array( array(title => 'Select Faculty...', value=>'0'), array(title => 'Faculty of Engineering Sciences', value=>'fes'), array(title => 'Faculty of Electronic Engineering', value=>'fee'), array(title => 'Faculty of Mechanical Engineering', value=>'fme'	), array(title => 'Faculty of Materials Engineering', value=>'fmse'), array(title => 'Faculty of Computer Engineering', value=>'fce'), array(title => 'Faculty of Computer Science', value=>'fcse')); 

if(isset($_GET['request'])) 
{
	$reset_request_select = mysql_query("SELECT * FROM `fes`.`".$tablename1."` WHERE `status` = '1' AND `forgotcode` = '".$_GET['request']."'") or die(mysql_error());
	$reset_request_found = mysql_num_rows($reset_request_select);

	if($reset_request_found == 0)
	{
		header('Location: ForgotYourPassword.php');
	}
	else
	{
		$reset['forgotcode']=$_GET['request'];
	}
}
else
{
	header('Location: ForgotYourPassword.php');
}
//actions
if($_POST['submit'])
{ 
	$reset['newpassword']=mysql_real_escape_string($_POST['newpassword']);
	$reset['confirmnewpassword']=mysql_real_escape_string($_POST['confirmnewpassword']);
	$reset['code']=mysql_real_escape_string($_POST['code']);
	if(!$reset['newpassword'])
	{
		$error['1']='*Please enter a new password.';
	}
	if(!$reset['confirmnewpassword'])
	{
		$error['2']='*Please enter the new password again.';
	}
	if($reset['newpassword'] && (strlen($reset['newpassword']) < 4 || strlen($reset['newpassword']) > 32))
	{
		$error['1']='*The username must be 4-32 characters long.';
	}
	
	if($reset['newpassword'] && $reset['confirmnewpassword'] && $reset['confirmnewpassword']!=$reset['newpassword'])
	{
		$error['2']='*The password you entered do not match, please enter again.';
	}
	
	if($reset['code']!=$_SESSION['code'])
	{
		$error['3']='*Please enter the code shown on left correctly.';
	}
	if(!$error)
	{
		$reset_request=mysql_fetch_array($reset_request_select);
		
		if($reset_request['status'])
		{
			//update password in user + forgot
			$reset['newpassword']=md5($reset['newpassword']);
			$reset_query= mysql_query("UPDATE `fes`.`".$tablename."` SET `password` = '".$reset['newpassword']."' WHERE `username`='".$reset_request['username']."' LIMIT 1;");	
			
			$success['ip'] = getRealIpAddr();
			$success['time'] = date("d/m/y : H:i:s", time());
			
			// delete request
			$delete_forgot_request=mysql_query("UPDATE `fes`.`".$tablename1."` SET `status`='0' WHERE `username`='".$reset_request['username']."'");
			
			
			$reset['query']=1;
			
			
			$_POST=NULL;
			$_SESSION=NULL;
		
		
		
		
		
		}
	}
}

$_SESSION['code']=rand(156712,987679)

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Reset Your Password</h1>
				<div id="form-wrapper">
				<?php 
				if(!$reset['query'])
				{
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">
							<span class="label">Enter the details to get reset link:</span>
							<tr><td><span class="label">New Password:</span></td><td><input type="password" name="newpassword"><span class="message"> '.$error['1'].'</span></td></tr>
							<tr><td><span class="label">Confirm New Password:</span></td><td><input type="password" name="confirmnewpassword"><span class="message"> '.$error['2'].'</span></td></tr>
							<tr><td><span class="label">Enter the Code Shown:</span></td><td><input type="text" name="code"><span class="label" style="font-size:16px;"><b> '.$_SESSION['code'].'</b></span><span class="message"> '.$error['3'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="submit" value="Submit" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>
						</form>
					</table>';
					}
				else if($reset['query'])
				{
					echo'<span class="label">Your password has been reset. Wait while you are being redirected to <a href="index.php">Login Page<a></span>';
					header('refresh:4;url="index.php"');
		
				}
				
				?>		
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
include('php/foot.php'); ?>