<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='course';
$page='Quizzes';
$tablename='quizzes';

require('php/functions.php');
if($_SESSION['current course code']){
	$pagetitle=$_SESSION['current course code'].' - '.$_SESSION['current course title'];
}

//extract entries from database
$entries=mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename."` ORDER BY `row id` DESC");
$entry=mysql_fetch_array($entries);

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Quizzes</h1>
				<?php if($_SESSION['course error']){
				echo '<h2>'.$_SESSION['course error'].'</h2>';
				}
				else{
				echo'<div id="sliding-list">';
				do
				{
					if(!$entry)
					{
					echo '
					<h2>No quiz announced.</h2>'; 
					} 
					else 
					{
					echo'
					<li class="closeitem expandable">'.$entry['title'].'</li>
					<ul class="listcontent">
						<li><h2>Title:</h2><p>'.$entry['title'].'</p></li>';
						if($entry['date'])
						{
						echo'<li><h2>Date:</h2><p>'.$entry['date'].'</p></li>';
						}
						if($entry['time'])
						{
						echo'<li><h2>Time:</h2><p>'.$entry['time'].'</p></li>';
						}
						if($entry['venue'])
						{
						echo'<li><h2>Venue:</h2><p>'.$entry['venue'].'</p></li>';
						}
						if($entry['syllabus'])
						{
						echo'<li><h2>Syllabus:</h2><p>'.$entry['syllabus'].'</p></li>';
						}
						if($entry['details'])
						{
						echo'<li><h2>Details:</h2><p>'.$entry['details'].'</p></li>';
						}
						if($entry['file'])
						{
						echo'
						<li><h2>Attachment:</h2><p><a href="../CourseWare/files/'.$_SESSION['current course code'].'/'.$entry['url'].'">'.$entry['file'].'</a></p></li>';
						}
						echo'<li><span class="posted">Posted on '.$entry['posted'].'</span></li>						
					</ul>';				
					}
				}
				while ($entry = mysql_fetch_array($entries));
								
				echo'
				</div><!--sliding-list-->';}?>
			
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$_SESSION['current course code']=NULL;
$_SESSION['course error']=NULL;

//destroy session current course
include('php/foot.php'); ?>
