<div id="midmodulebox">
	<div id="midmodule-dynamic" >
    	<ul id="midmodule-content" style="background: url(../images/MidModule/<?php echo rawurlencode($sectionheading);?>.jpg);">
			
			<div class="links">
            <?php 
			if($menu==1) //about fes
			{ echo"
				<li><a href='".$path."About FES/Overview.php'>Overview</a></li>
           		<li><a href='".$path."About FES/MissionAndObjectives.php'>Mission and Objectives</a></li>
           		<li><a href='".$path."About FES/DeansMessage.php'>Dean's Message</a></li>
                <li><a href='".$path."About FES/WhatIsEngSci.php'>What is Engineering Sciences?</a></li>";
			}
			
			if($menu==2) //join fes
			{ echo'
				 <li><a href="#">Undergraduate Student</a></li>
                <li><a href="#">Graduate Student</a></li>
                <li><a href="#">Faculty Member</a></li>';
			}
			
			if($menu==3) //academics
			{ echo'
				<li><a href="'.$path.'Academics/EngSciProgram.php">Engineering Sciences Program</a></li>
                <li><a href="'.$path.'Academics/DegreePlan.php">Degree Plan</a></li>
				<li><a href="'.$path.'Academics/Courses.php">Courses</a></li>
                <li><a href="'.$path.'Academics/Specializations.php">Specializations</a></li>';
			}
							
			if($menu==4) //labs
			{ echo'
				<li><a href="'.$path.'Laboratories/TeachingLabs.php">Teaching Laboratories</a></li>
                <li><a href="'.$path.'Laboratories/LabManuals.php">Student Lab Manuals</a></li>';
			}
			
			if($menu==5) //research
			{ echo'
				<li><a href="'.$path.'Research/ResearchAreas.php">Research Areas</a></li>
                <li><a href="'.$path.'Research/ResearchLabs.php">Research Labs</a></li>';
			}
			
			if($menu==6) //fes council
			{ echo'
				<li><a href="'.$path.'FES Council/Overview.php">Overview</a></li>
                <li><a href="'.$path.'FES Council/ExecutiveCouncil.php">Executive Council</a></li>
                <li><a href="'.$path.'FES Council/Membership.php">Membership</a></li>';
			}
			
			if($menu==7) //about giki
			{ 
			}
			
			if($menu=='a') //faculty
			{ echo'
				<li><a href="../Faculty/Dean.php">Dean</a></li>
                <li><a href="../Faculty/Fulltime.php">Fulltime Faculty</a></li>
                <li><a href="../Faculty/ResearchAssociate.php">Research Associates</a></li>
				<li><a href="../Faculty/Visiting.php">Visiting Faculty</a></li>
                <li><a href="../Faculty/LabEngineers.php">Lab Engineers</a></li>';
			}
			
			if($menu=='b') //current students
			{ echo'
				<li><a href="../CourseWare/">CourseWare</a></li>
                <li><a href="../Students/DeansHonourRoll.php">Dean'."'".'s Honour Roll</a></li>
                <li><a href="../Students/BatchLists.php">Batch Listings</a></li>
                <li><a href="../Students/Projects.php">Projects</a></li>
                <li><a href="../Students/Societies.php">Societies</a></li>';
			}
			
			
			if($menu=='d') //industry and careers
			{ echo'
				<li><a href="#">Career Options</a></li>
                <li><a href="../Industry And Careers/Internships.php">Internships</a></li>
                <li><a href="#">Job Oppurtunities</a></li>
                <li><a href="#">Further Study</a></li>
                <li><a href="../Industry And Careers/Resumes.php">Resumes</a></li>';
			}
			
			if($menu=='e') //news
			{ echo'
				';
			}
			
			if($menu=='f') //seminars
			{ echo'
				';
			}
			
			?>
        	
            </div>
		</ul>
	</div>	
    
    <div id="midmodule-static">
		<ul id="midmodule-content" class="midmodule-content1">
			<div class="links">
          		<li><a href="../Faculty/Dean.php">Dean</a></li>
                <li><a href="../Faculty/Fulltime.php">Fulltime Faculty</a></li>
                <li><a href="../Faculty/ResearchAssociate.php">Research Associates</a></li>
				<li><a href="../Faculty/Visiting.php">Visiting Faculty</a></li>
                <li><a href="../Faculty/LabEngineers.php">Lab Engineers</a></li>
            </div>
		</ul>
		
        <ul id="midmodule-content" class="midmodule-content2">
			<div class="links">
           		<li><a href="../CourseWare/">CourseWare</a></li>
                <li><a href="../Students/DeansHonourRoll.php">Dean's Honour Roll</a></li>
                <li><a href="../Students/BatchLists.php">Batch Listings</a></li>
                <li><a href="../Students/Projects.php">Projects</a></li>
                <li><a href="../Students/Societies.php">Societies</a></li>
            </div>
		</ul>
        
        <ul id="midmodule-content" class="midmodule-content3">
			<div class="links">
                <li><a href="#">What is Engineering Sciences?</a></li>
                <li><a href="../Academics/EngSciProgram.php">Engineering Sciences Program at FES</a></li>
                <li><a href="#">Undergraduate Studies</a></li>
                <li><a href="#">Graduate Studies</a></li>
            </div>
		</ul>
		
		<ul id="midmodule-content" class="midmodule-content4">
			<div class="links">
                <li><a href="#">Career Options</a></li>
                <li><a href="../Industry And Careers/Internships.php">Internships</a></li>
                <li><a href="#">Job Oppurtunities</a></li>
                <li><a href="#">Further Study</a></li>
                <li><a href="../Industry And Careers/Resumes.php">Resumes</a></li>
            </div>
		</ul>
	</div>
    
    <div id="midmodule-menu">
		<ul class="buttons">
			<li class="button1"> <a href="#">Faculty and Staff</a> </li>
			<li class="button2"> <a href="#">Current Students</a> </li>
			<li class="button3"> <a href="#">Prospective Students</a> </li>
			<li class="button4"> <a href="#">Industry and Careers</a> </li>
		</ul>
	</div>
</div>
