<?php echo     	'';
     switch($pageheading){
	case 'Teaching Labs':
	echo"
          	<h1>Teaching Laboratories</h1>
			<p>The Faculty of Engineering Sciences has a large number of teaching and research laboratories. The teaching laboratories include</p>
			<p>
			> Mechanics Laboratory<br>
> Electricity and Magnetism Laboratory<br>
> Circuit Analysis Laboratory<br>
> Logic Design Laboratory<br>
> Electronics Laboratory<br>
> Computer Architecture Laboratory<br>
> Microprocessor/Microcontroller Interfacing Laboratory<br>
> Signals and Systems Laboratory<br>
> Engineering Instrumentation Laboratory<br>
> Simulation Laboratory<br>
> Semiconductor Laboratory<br>
> Lasers and Optics Laboratory<br>
> Modern Physics Laboratory</p>
<p><a href='LabManuals.php'>Click Here</a> to download student lab manuals.</p>";
		   break;
		   
	case 'Lab Manuals':
	echo"<h1>Student Lab Manuals</h1>
				<table class='inner'>
				<tr><td>PH101L</td><td> Mechanics Laboratory</td><td><a href='Lab Manuals/Mechanics Lab manual -- 2013.pdf'>Download</a></td></tr>
				<tr><td>PH102L</td><td> Electricity and Magnetism Laboratory</td><td><a href='Lab Manuals/Electricity and Magnetism Lab manual -- 2014.doc'>Download</a></td></tr>
				<tr><td>ES211L</td><td> Circuit Analysis Laboratory</td><td><a href='Lab Manuals/Circuit I Lab Manual -- 2013.pdf'>Download</a></td></tr>
				<tr><td>ES212L</td><td> Logic Design Laboratory</td><td><a href='Lab Manuals/Logic Design Lab Manual -- 2013.pdf'>Download</a></td></tr>
				<tr><td>ES231L</td><td> Electronics Laboratory</td><td><a href='Lab Manuals/Electronics I Lab Manual-- 2013.pdf'>Download</a></td></tr>
				<tr><td>ES213L</td><td> Computer Architecture Laboratory</td><td><a href='Lab Manuals/Computer Architecture Lab Manual -- 2013.docx'>Download</a></td></tr>
				<tr><td>ES314L</td><td> Microprocessor/Microcontroller Interfacing Laboratory</td><td><a href='Lab Manuals/Microprocessor Lab Manual -- 2013.pdf'>Download</a></td></tr>
				<tr><td>ES322L</td><td> Signals and Systems Laboratory</td><td><a href='Lab Manuals/Signals Lab Manual -- 2013.pdf'>Download</a></td></tr>
				<tr><td>ES451L</td><td> Engineering Instrumentation Laboratory</td><td><a href='Lab Manuals/Instrumentation Lab Manual --2013.pdf'>Download</a></td></tr>
				<tr><td>ES441L</td><td> Simulation Laboratory</td><td><a href='Lab Manuals/Simulation Lab Manual -- 2014.pdf'>Download</a></td></tr>
				<tr><td>ES462L</td><td> Semiconductor Laboratory</td><td><a href='Lab Manuals/Semiconductors Lab Manual -- 2013.docx'>Download</a></td></tr>
				<tr><td>ES471L</td><td> Lasers and Optics Laboratory.</td><td><a href='Lab Manuals/Optics Manual -- 2013.pdf'>Download</a></td></tr>
				</table>
			<h1>Student Lab Softwares</h1>
				<table class='inner'>
				<tr><td>SIMUL8 Simulation Software</td><td><a href='Softwares/SIMUL8.zip'>Download</a></td></tr>
				</table>";
		   break;
	
		   
}
?>