<?php echo     	'';
     switch($pageheading){
	case 'Engineering Sciences Program':
	echo"
          	<h1>Engineering Sciences Program</h1>
			<p>The Faculty of Engineering Sciences offers holistic perspective of the fundamental sciences and provides opportunities to integrate systems from 			different disciplines. The courses provide a high quality and broad based coverage of multi-disciplinary engineering using Mathematics, Physics, Computer Science 			and Engineering to conduct research and design innovative engineering solution. The course offers excellent job opportunities in industrial sector and in R&D organization providing a launching platform to conduct graduate studies in the globally emerging field of Engineering Sciences.</p>
			<p>The Faculty offers a 4-year degree program leading to Bachelor of Science in Engineering Sciences which encompasses some of the modern fields of engineering such 			as:</p>

			<p>> Lasers and Electro-Optics<br>
			> Semiconductors and Superconducting Devices<br>
			> Modelling and Simulations</p>

			<p>The faculty also offers MS and PHD degree programs in applied mathematics and applied physics.</p>";
		   break;
		   
	case 'Courses':
	echo"
            <h1>Courses</h1>
			<p>Courses offered by Faculty of Engineering Sciences are as follows:</p>";
			include('content-courses.php');
		   break;		
		   
	case 'Specializations':
	echo"
            <h1>Specializations</h1>
			
			<h2>Lasers and Optoelectronics</h2>
			<p>Lasers and Optoelectronics deals with photonic technologies. Technologies related to Photonics such as lasers, optoelectronics, fiber optics, and optical communicaton have emerged as very useful andwidely used fields.Photonics is seen as a replacement for conventional Electronics in many major areas including medicine, communications, manufacturing, industrial automation and computers. Students with specialization in Lasers and optoelectronics make careers in telecommunication, nanotechnology, lasers, health care, industrial automation and controls.</p>
			
			<h2>Modelling and Simulatons</h2>
			<p>Modelling and simulations have become an integral part in the designing and manufacturing fields. This decipline offers the tools and knowledge required to design mathematical models of actual, as well as theoretical systems, digitally executing them on computers, and analyzing the execution output. The area of computer simmulations was developed hand-in-hand with the growth of computer industry. Simulations are use in every sector of today's industry, which include airplanes, missiles, communication networks, transport vehicles, health systems, whether forcasting, financial manrkets, statistical studies, entertainment industry, service system (post offices, toll booths), and many others. People with experties in modelling and simulations have job opportunities in almost all industries and service sectors.</p>
		
			<h2>Semiconductors and Superconducting devices</h2>
			<p>The entire electronics and computer industry, without which today's world cannot be imagined, are based on semiconductor devices. Semiconductor technologies have powerful applications in many industries, eg modern electronics, communications, defence industry, automobile industry, medical diagnosis equipment, aerospace industry, and many more. Infact, in today's world, semiconductor technologies are used in almost every field immaginable.</p>
			<p>Semiconductors are also the core of nanotechnology. This technology has the potential to revolutionize our lives. Nano tech researches have shown a lot of prommise in many different fields including smaller and faster electronics, large memory and storage for computers, efficienct energy conversion systems, security systems through development of nanoscale bio and chemical detection systems, and nano materials.</p>
			<p>Imagine transporting electricity halfway around the world without losing any energy or unimaginably fast computer circuits with no resistance. Superconductivity is a remarkable phenomenon which will enable us to achieve many such goals, and a lot of research is being done in this field.</p>
			";
			break;
			
	case 'Degree Plan':
	echo"
            <h1>Degree Plan</h1>
			<p>For the BS degree in Engineering Sciences a student is required to complete 135 credit hours by completing the following courses:</p>
			
			<h2>First Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>MT101</td><td>Calculus I</td><td>3</td></tr>
			<tr><td>PH101</td><td>Mechanics</td><td>3</td></tr>
			<tr><td>MM101</td><td>Industrial Chemistry</td><td>3</td></tr>
			<tr><td>HM101</td><td>English and Study Skills</td><td>3</td></tr>
			<tr><td>CS101</td><td>Introduction to Computing</td><td>2</td></tr>
			<tr><td>ME101</td><td>Workshop Practice</td><td>1</td></tr>
			<tr><td>PH101L</td><td>Mechanics Lab</td><td>1</td></tr>
			<tr><td>CS101L</td><td>Computing Lab</td><td>1</td></tr>
			</table>
			
			<h2>Second Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>MT102</td><td>Calculus II</td><td>3</td></tr>
			<tr><td>PH102</td><td>Electricity and Magnetism</td><td>3</td></tr>
			<tr><td>MM102</td><td>Introduction to Engineering Materials</td><td>3</td></tr>
			<tr><td>HM102</td><td>Technical Report Writting</td><td>3</td></tr>
			<tr><td>ME102</td><td>Engineering Graphics</td><td>2</td></tr>
			<tr><td>PH102L</td><td>Electricity and Magnerism Lab</td><td>1</td></tr>
			<tr><td>MM141L</td><td>Materials Lab I</td><td>1</td></tr>
			<tr><td>CS102</td><td>Intensive Programming Lab</td><td>1</td></tr>
			</table>
			
			<h2>Third Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES211</td><td>Circuit Analysis I</td><td>3</td></tr>
			<tr><td>ES212</td><td>Logic Design</td><td>3</td></tr>
			<tr><td>ES221</td><td>Data Structures and Algorithm Analysis</td><td>3</td></tr>
			<tr><td>MT201</td><td>Differential Equations</td><td>3</td></tr>
			<tr><td>HM211</td><td>Pakistan and Islamic Studies</td><td>3</td></tr>
			<tr><td>ES211L</td><td>Circuit Analysis Lab</td><td>1</td></tr>
			<tr><td>ES212L</td><td>Logic Design Lab</td><td>1</td></tr>
			</table>
			
			<h2>Fourth Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES214</td><td>Circuit Analysis II</td><td>3</td></tr>
			<tr><td>ES231</td><td>Electronics I</td><td>3</td></tr>
			<tr><td>ES213</td><td>Computer Architecture</td><td>3</td></tr>
			<tr><td>ES222</td><td>Operating Systems</td><td>3</td></tr>
			<tr><td>ES202</td><td>Engineering Statistics</td><td>3</td></tr>
			<tr><td>ES231L</td><td>Electronics I Lab</td><td>1</td></tr>
			<tr><td>ES213L</td><td>Computer Architecture Lab</td><td>1</td></tr>
			<tr><td>ES222L</td><td>Operating Systems Lab</td><td>1</td></tr>
			</table>
			
			<h2>Fifth Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES332</td><td>Signals and Systems</td><td>3</td></tr>
			<tr><td>ES314</td><td>Microprocessor Interfacing</td><td>3</td></tr>
			<tr><td>ES331</td><td>Themodynamics</td><td>3</td></tr>
			<tr><td>ES304</td><td>Linear Algebra</td><td>3</td></tr>
			<tr><td>HM321</td><td>Sociology and Human Behavior</td><td>3</td></tr>
			<tr><td>ES332L</td><td>Signals and Systems Lab</td><td>1</td></tr>
			<tr><td>ES314L</td><td>Microprocessor Interfacing Lab</td><td>1</td></tr>
			</table>
			
			<h2>Sixth Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES333</td><td>Fluid Mechanics</td><td>3</td></tr>
			<tr><td>ES371</td><td>Engineering Electromagnetics</td><td>3</td></tr>
			<tr><td>ES341</td><td>Numerical Analysis</td><td>3</td></tr>
			<tr><td>ES3XX</td><td>Faculty Elective (Specialization)</td><td>3</td></tr>
			<tr><td>XX3XX</td><td>Interfaculty Elective</td><td>3</td></tr>
			<tr><td>HM322</td><td>Ethics and Legal Dimension of Engineering</td><td>3</td></tr>
			</table>
			
			<h2>Seventh Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES451</td><td>Instrumentation</td><td>3</td></tr>
			<tr><td>ES462</td><td>Semiconductor Materials and Devices</td><td>3</td></tr>
			<tr><td>ES4xx</td><td>Faculty Elective (Specialization)</td><td>3</td></tr>
			<tr><td>MS4XX</td><td>General Management Elective</td><td>3</td></tr>
			<tr><td>ES481</td><td>Senior Design Project Part I</td><td>3</td></tr>
			<tr><td>ES451L</td><td>Instrumentation Lab</td><td>1</td></tr>
			<tr><td>ES4XXL</td><td>Specialization Lab</td><td>1</td></tr>
			
			</table>
			
			<h2>Eighth Semester</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES4XX</td><td>Faculty Elective (Specialization)</td><td>3</td></tr>
			<tr><td>ES4XX</td><td>Faculty Elective (Specialization)</td><td>3</td></tr>
			<tr><td>XX4XX</td><td>Interfaculty Elective</td><td>3</td></tr>
			<tr><td>MS4XX</td><td>General Management Elective</td><td>3</td></tr>
			<tr><td>ES482</td><td>Senior Design Project Part II</td><td>3</td></tr>
			</table>
			
			<h1>Specialization Requirements</h1>
			
			<h2>Lasers and Optoelectronics</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES376</td><td>Optical Engineering</td><td>3</td></tr>
			<tr><td>ES472</td><td>Lasers and Applications</td><td>3</td></tr>
			<tr><td>ES474</td><td>Optoelectronics</td><td>3</td></tr>
			<tr><td>ES475</td><td>Optical Communication and Computing</td><td>3</td></tr>
			<tr><td>ES471L</td><td>Optics Lab</td><td>1</td></tr>
			</table>
			
			<h2>Modelling and Simulation</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES342</td><td>Modelling Processes</td><td>3</td></tr>
			<tr><td>ES444</td><td>Computer Simulation Methods</td><td>3</td></tr>
			<tr><td>ES445</td><td>Heat Transfer and Modelling</td><td>3</td></tr>
			<tr><td>ES446</td><td>Optimization Modelling</td><td>3</td></tr>
			<tr><td>ES441L</td><td>Simulation Lab</td><td>1</td></tr>
			</table>
			
			<h2>Semiconductors and Superconducting Devices</h2>
			<table class='inner'>
			<tr><td width='100px'><b>Code</b></td><td width='270px'><b>Course Name</b></td><td width='100px'><b>Credit Hours</b></td></tr>
			<tr><td>ES361</td><td>Solid State Electronics</td><td>3</td></tr>
			<tr><td>ES463</td><td>Electronic and Magnetic Materials</td><td>3</td></tr>
			<tr><td>ES464</td><td>Characterization of Materials</td><td>3</td></tr>
			<tr><td>ES465</td><td>Semiconductor Devices and Applications</td><td>3</td></tr>
			<tr><td>ES462L</td><td>Semiconductor and Devices Lab</td><td>1</td></tr>
			</table>
			";
			break;
		   
}
?>